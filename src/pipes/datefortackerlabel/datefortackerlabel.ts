import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datefortackerlabel',
})
export class DatefortackerlabelPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    let m = moment(value).format('MMMM YYYY');
    return m;
  }
}
