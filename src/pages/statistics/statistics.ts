import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, Loading, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-statistics',
  templateUrl: 'statistics.html',
})
export class StatisticsPage {

  lets : any;
  dataPlayersStatistics : any;
  teamId = '79';
  loading : any;
  datasCompe : any;
  dataTest : any;
  dataFinal : any;
  compeId : any; 

  constructor(public api: ApiProvider, private LoadingCtrl: LoadingController,  private alertCtrl: AlertController, public http: Http, private platform: Platform, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    
    this.presentLoader();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatisticsPage');
    this.viewCtrl.setBackButtonText('');
   
  }

    // load animation gif
    presentLoader() {
      this.loading = this.LoadingCtrl.create({
        spinner: 'hide',
        content: ` <div class="custom-spinner-container">
        <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
      </div>`
      });
      this.loading.present();
      this.getPlayersStatistics();
    }

  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

   //get genius matches with dismiss loading gif
   getPlayersStatistics(){
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
      //console.log(this.compeId);

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/statistics/personcompetition/competitions/'+  this.compeId +'/teams/'+ this.teamId +'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=30')
      .map(res => res.json())
      .subscribe(data => {
        this.dataPlayersStatistics = data.response.data;
        this.loading.dismiss();
        console.log(this.dataPlayersStatistics);
      }, err => {
        this.presentConfirm();
        console.log(err);
      
      });
    })
    .catch( error => {
    console.log('error');
    this.presentConfirm();  
    })
  }

  //player first name and lastname convert 
  name(dataPlayersStatistic) {
    let fname = dataPlayersStatistic.firstName;
    let lname = dataPlayersStatistic.familyName;
    let g = lname.toString();
    let pota = lname.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    let f = fname.toString();
    let n = f.substr(0, 1);
    
    return n + '. ' + upper;
  }

    // one decimal place min
  decimalMpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sMinutesAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place ppg
  decimalCareerPpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sPointsAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place apg
  decimalCareerApg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sAssistsAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place rpg
  decimalCareerRpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sReboundsTotalAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place bpg
  decimalCareerBpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sBlocksAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place spg
  decimalCareerSpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sStealsAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place tpg
  decimalCareerTpg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sTurnoversAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  // one decimal place fppg
  decimalCareerFppg(dataPlayersStatistic) {
    let a = dataPlayersStatistic.sFoulsPersonalAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  //player fg convert
  fieldgoalCareer(dataPlayersStatistic) {
    let fg =  dataPlayersStatistic.sFieldGoalsPercentage;
    let n = fg.toString();
    if(n.length < 2) {
        if(n == 1) {
          return n + '00';
        } else {
          return n;
        }
    } else {
      let result = n.slice(2, 4);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  threepointsCareer(dataPlayersStatistic) {
    let tp =  dataPlayersStatistic.sThreePointersPercentage;
    let n = tp.toString();
    if(n.length < 2) {
        if(n == 1) {
          return n + '00';
        } else {
          return n;
        }
    } else {
      let result = n.slice(2, 4);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  freethrowsCareer(dataPlayersStatistic) {
    let tp =  dataPlayersStatistic.sFreeThrowsPercentage;
    let n = tp.toString();
    if(n.length < 2) {
        if(n == 1) {
          return n + '00';
        } else {
          return n;
        }
    } else {
      let result = n.slice(2, 4);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

}
