import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, Loading, AlertController } from 'ionic-angular';
import { StatisticsPage } from '../statistics/statistics';
import { RosterPage } from '../roster/roster';
import { TrackerPage } from '../tracker/tracker';
import { StandingsPage } from '../standings/standings';
import { PlayerDetailPage } from '../player-detail/player-detail';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-team',
  templateUrl: 'team.html',
})
export class TeamPage {

  lets : any;
  teams : any=[];
  teamID = '79';
  teamStats : any;
  teamLastFives : any=[];
  teamLastGames : any;
  teamLastFinalGames : any;
  tututs : any;
  lastGames : any;
  loading : any;
  teamStandings : any; 
  stands : any;
  teamFinalStandings : any;
  dataPoints : any;
  dataRebounds : any;
  dataAssists : any;

  passpoints : any;
  passassists : any;
  passrebounds : any;
  datasCompe : any;
  dataTest : any;
  dataFinal : any;
  compeId : any;
  datasCompegetPointsLeader : any;
  dataTestgetPointsLeader : any;
  dataFinalgetPointsLeader : any;
  compeIdgetPointsLeader : any;
  shita : any;
  huy : any;
  dateTestNow: any;

  constructor(public api: ApiProvider, private alertCtrl: AlertController, private LoadingCtrl: LoadingController, public http: Http,private platform: Platform, public navCtrl: NavController, public navParams: NavParams) {
    this.presentLoader() 
  }

  //go to statistics page
  statistics() {
    this.navCtrl.push(StatisticsPage);
  }

  //go to roster page
  roster() {
    this.navCtrl.push(RosterPage);
  }

  //go to tracker page
  tracker() {
    this.navCtrl.push(TrackerPage);
  }

  //go to tracker page
  standings() {
    this.navCtrl.push(StandingsPage);
  }

  //go to player details page
  playerDetailsPoints(dataPoint) {
    this.passpoints = dataPoint.personId;
    this.navCtrl.push(PlayerDetailPage, { post: this.passpoints });
  }

  playerDetailsAssists(dataAssist) {
    this.passassists = dataAssist.personId;
    this.navCtrl.push(PlayerDetailPage, { post:this.passassists });
  }

  playerDetailsRebounds(dataRebound) {
    this.passrebounds = dataRebound.personId;
    this.navCtrl.push(PlayerDetailPage, { post:this.passrebounds });
  }
  
  //get genius points lead in team
  getPointsLeader(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
     
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/leaders/sPointsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId='+ this.teamID +'&periodNumber=0&limit=1')
      .map(res => res.json())
      .subscribe(data => {
        this.dataPoints = data.response.data;  
      
      }, err => {
        console.log(err); 
      });
    })
    .catch( error => {
      console.log('error');
    })
  }

  //get genius rebounds lead in team
  getReboundsLeader(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;

        if (this.platform.is('mobile')) {
          this.lets = "https://api.wh.geniussports.com/v1";
        } else {
          this.lets = "/v1";
        }

        this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/leaders/sReboundsTotalAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId='+ this.teamID +'&periodNumber=0&limit=1')
        .map(res => res.json())
        .subscribe(data => {
          this.dataRebounds = data.response.data;  
          this.loading.dismiss();

        }, err => {
          console.log(err); 
        }); 
    })
    .catch( error => {
      console.log('error');
      this.presentConfirm();  
    })
  }


  //get genius assists lead in team
  getAssistsLeader(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
        if (this.platform.is('mobile')) {
          this.lets = "https://api.wh.geniussports.com/v1";
        } else {
          this.lets = "/v1";
        }

        this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/leaders/sAssistsAverage?ak=eebd8ae256142ac3fd24bd2003d28782&teamId='+ this.teamID +'&periodNumber=0&limit=1')
        .map(res => res.json())
        .subscribe(data => {
          this.dataAssists = data.response.data;  
        
        }, err => {
          console.log(err); 
        });
    })
    .catch( error => {
      console.log('error');
    })
  }

  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.getTeamStats();
    this.getTeamLastFive();
    this.getTeamLastGames();
    this.getTeamInfo();
    this.getStanding();
    this.getPointsLeader();
    this.getReboundsLeader();
    this.getAssistsLeader();
  }

  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

 
  //get genius matches team info
  getTeamInfo(){ 
    
    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/teams/'+ this.teamID +'?ak=eebd8ae256142ac3fd24bd2003d28782')
    .map(res => res.json())
    .subscribe(data => {
      this.teams[0] = data.response.data;  
      
    }, err => {
      console.log(err); 
     
    });
  }

  // convert country code to lowercase
  shitCountry(team) {
    let a = team.countryCode.toString();
    let b = a.toLowerCase();
    return b;
  }

  //get genius matches team stats latest competition
  getTeamStats(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
      
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
      
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/statistics/teamcompetition/competitions/'+ this.compeId +'/teams/'+ this.teamID +'?ak=eebd8ae256142ac3fd24bd2003d28782&limit=1')
      .map(res => res.json())
      .subscribe(data => {
        this.teamStats = data.response.data;  
       
      }, err => {
        console.log(err); 
      });
    })
    .catch( error => {
    console.log('error');
    })
  }

  //get genius standings specific team for l5
  getTeamLastFive(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (b.year < a.year) ? -1 : (b.year > a.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
     

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/standings/competitors/'+ this.teamID +'?ak=eebd8ae256142ac3fd24bd2003d28782&roundNumber=1')
      .map(res => res.json())
      .subscribe(data => {
        this.teamLastFives[0] = data.response.data;  
      }, err => {
        console.log(err); 
      });
    })
    .catch( error => {
    console.log('error'); 
    })
  }

   //get genius standings
   getStanding(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
     

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/standings?ak=eebd8ae256142ac3fd24bd2003d28782')
      .map(res => res.json())
      .subscribe(data => {
        this.teamStandings = data.response.data;  

        //sort api to position
        // this.stands = JSON.parse(JSON.stringify(this.teamStandings));
        // this.teamFinalStandings =  this.stands.sort(function(a, b) {
        //     return parseInt(a.position) - parseInt(b.position);
        // });

        this.stands = JSON.parse(JSON.stringify(this.teamStandings));
        this.teamFinalStandings =  this.stands.sort(function(a, b) {
            return (parseFloat(b.percentageWon) < parseFloat(a.percentageWon)) ? -1 : (parseFloat(b.percentageWon) > parseFloat(a.percentageWon)) ? 1 : 0;
        });
 

        
      }, err => {
        console.log(err); 
      });
    })
    .catch( error => {
    console.log('error'); 
    })
  }

   //get genius last games
   getTeamLastGames(){ 
    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
      
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      var date = new Date();

      let year = date.getFullYear();
      let month = date.getMonth()+1;
      let day = date.getDate();
   
      this.dateTestNow = year + '-' + month + '-' + day;

      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&toDate='+ this.dateTestNow +'&teamId='+ this.teamID +'&limit=100')
      .map(res => res.json())
      .subscribe(data => {
        this.teamLastGames = data.response.data; 

        //sort api to date
        this.teamLastFinalGames = JSON.parse(JSON.stringify(this.teamLastGames));
        this.tututs =  this.teamLastFinalGames.sort(function(a, b) {
            return parseInt(b.matchNumber) - parseInt(a.matchNumber);
        });
        
        this.lastGames = this.tututs.slice(0, 3);
        console.log( this.lastGames, 'test last games');
      }, err => {
        console.log(err); 
      });
    })
    .catch( error => {
    console.log('error');
    })
  }

 

  //one decimal places ppg
  decimalPpg(teamStat) {
    let a =  teamStat.sPointsAverage;
    let b = a.toFixed(1);
    return b;
  }

  // one decimal places rpg
  decimalRpg(teamStat) {
    let a = teamStat.sReboundsTotalAverage;
    let b = a.toFixed(1);
    return b;
  }

  // one decimal places apg
  decimalApg(teamStat) {
    let a = teamStat.sAssistsAverage;
    let b = a.toFixed(1);
    return b;
  }

  // one decimal places spg
  decimalSpg(teamStat) {
    let a = teamStat.sStealsAverage;
    let b = a.toFixed(1);
    return b;
  }

  // one decimal places bpg
  decimalBpg(teamStat) {
    let a = teamStat.sBlocksAverage;
    let b = a.toFixed(1);
    return b;
  }

  //last5 stats
  lastFive(teamLastFive) {
    let a = teamLastFive.last5;
    // let w = a.split("W").length - 1;
    // let l = a.split("L").length - 1;
    // return w + '-' + l;
    return a;
  }

  //if slingers home convert to int and compare
  ishome(lastGame) {
    let a = parseInt(lastGame.competitors[0].scoreString);
    let b = parseInt(lastGame.competitors[1].scoreString);
    
    if(a > b) {
      return lastGame.competitors[0].scoreString + '-' + lastGame.competitors[1].scoreString + ' W';
    } else {
      return lastGame.competitors[0].scoreString + '-' + lastGame.competitors[1].scoreString + ' L';
    }
  }

   //if slingers not home convert to int and compare
   isaway(lastGame) {
    let a = parseInt(lastGame.competitors[1].scoreString);
    let b = parseInt(lastGame.competitors[0].scoreString);
    
    if(a > b) {
      return lastGame.competitors[1].scoreString + '-' + lastGame.competitors[0].scoreString + ' W';
    } else {
      return lastGame.competitors[1].scoreString + '-' + lastGame.competitors[0].scoreString + ' L';
    }
  }

  //standing l5
  lastpayb(teamFinalStanding) {
    let a = teamFinalStanding.last5;
    let w = a.split("W").length - 1;
    let l = a.split("L").length - 1;
    return w + '-' + l;
  }

  //standing streak
  streak(teamFinalStanding) {
    let a = teamFinalStanding.last5;
    let lastChar = a.substr(a.length - 1);
    if(teamFinalStanding.streak.length > 1) {
      let res = teamFinalStanding.streak.slice(1, 3);
      return lastChar + res;
    } else {
      return lastChar + teamFinalStanding.streak;
    }
   
  }

  //standing win percentage 
  winpercentage(teamFinalStanding) {

    if(teamFinalStanding.played > 0 ) {
      if(teamFinalStanding.won == 0) {
          
          return '.000';
          
      } else {
          
          let totalHa = teamFinalStanding.won / teamFinalStanding.played;
          let totalHaString = totalHa.toString();
          let totalHaTest = totalHaString.length;
          
          if(totalHaTest == 1) {
              return '100';
          } else {
              
              let shitHa = totalHa.toString();
              let remove = shitHa.substring(1, 5);
              let yeah = remove.length;
              
              if(yeah == 2) {

                return remove + '00';
                
            } 
            
            if(yeah == 3) {

                return remove + '0';

            }

            if(yeah == 4) {

              return remove;

            }
          
          }
          
      }
      
  } else {
      
      return '.000';
      
  }

}



  //team stat fg percentage 
  fgpercentage(teamStat) {
    let a = teamStat.sFieldGoalsMade;
    let b = teamStat.sFieldGoalsAttempted;
    let res = a/b;
    let n = res.toFixed(3);
    let shit = n.toString();
    let remove = shit.substring(1);

    return remove;
  }

  //team stat fg 3pt% 
  threeptpercentage(teamStat) {
    let a = teamStat.sThreePointersMade;
    let b = teamStat.sThreePointersAttempted;
    let res = a/b;
    let n = res.toFixed(3);
    let shit = n.toString();
    let remove = shit.substring(1);

    return remove;
  }

  //team stat ft%
  ftpercentage(teamStat) {
    let a = teamStat.sFreeThrowsMade;
    let b = teamStat.sFreeThrowsAttempted;
    let res = a/b;
    let n = res.toFixed(3);
    let shit = n.toString();
    let remove = shit.substring(1);

    return remove;
  }

  //team lead points 
  pointLeader(dataPoint) {
    let a = dataPoint.sPointsAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  //team lead points cut firstname 
  leadPointsFirstname(dataPoint) {
    let c = dataPoint.firstName;
    let d = c.split(" ");
    let e = d[0];
    return e;
  }

  //lead points player last name convert 
  leadPointsLastname(dataPoint) {
    let get = dataPoint.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

   //team lead assists 
   assistsLeader(dataAssist) {
    let a = dataAssist.sAssistsAverage;
    let b = a.toFixed(2).slice(0,-1);
    return b;
  }

  //team lead assists cut firstname 
  leadAssistsFirstname(dataAssist) {
    let c = dataAssist.firstName;
    let d = c.split(" ");
    let e = d[0];
    return e;
  }

  //lead assists player last name convert 
  leadAssistsLastname(dataAssist) {
    let get = dataAssist.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

     //team lead rebounds 
     reboundsLeader(dataRebound) {
      let a = dataRebound.sReboundsTotalAverage;
      let b = a.toFixed(2).slice(0,-1);
      return b;
    }
  
    //team lead rebounds cut firstname 
    leadReboundsFirstname(dataRebound) {
      let c = dataRebound.firstName;
      let d = c.split(" ");
      let e = d[0];
      return e;
    }
  
    //lead rebounds player last name convert 
    leadReboundsLastname(dataRebound) {
      let get = dataRebound.familyName;
      let g = get.toString();
      let pota = get.toLowerCase();
      let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
      
      return upper;
    }

  
  

}
