import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TeamPage } from './team';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(TeamPage),
  ],
})
export class TeamPageModule {}
