import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StatusBar } from '@ionic-native/status-bar';
/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {

  post : any;
  url : any;
  orientation: string;

  constructor(private platform: Platform, statusBar: StatusBar,private screenOrientation: ScreenOrientation, private iab: InAppBrowser, public navCtrl: NavController, public navParams: NavParams, public view: ViewController) {
    // this.post = navParams.get('post');
    // this.url = "https://www.youtube.com/embed/"+ this.post +"?rel=0;&autoplay=1";
    this.url = "https://www.youtube.com/embed/RXENKe1Cvvg?rel=0;&autoplay=1";
    // console.log(this.url);
    // this.iab.create('https://www.youtube.com/watch?v=RXENKe1Cvvg');
    this.screenOrientation.unlock();
  }

   

  closeModal() {

    this.view.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

}
