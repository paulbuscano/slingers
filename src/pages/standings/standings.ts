import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, Loading, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-standings',
  templateUrl: 'standings.html',
})
export class StandingsPage {

  lets : any;
  teamStandings : any;
  stands : any;
  teamFinalStandings : any;
  loading : any;
  datas : any;
  dataTest : any;
  dataFinal : any;
  compeId : any;
  ulan:any=[];

  constructor(public api: ApiProvider, private alertCtrl: AlertController, private LoadingCtrl: LoadingController, public http: Http,private platform: Platform, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.presentLoader();
    this.latestCompetitionId();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StandingsPage');
    this.viewCtrl.setBackButtonText('');
  }

//get latest competition id
latestCompetitionId(){
  this.api.loadLatestCompetitionId()
  .then( data => {
    this.datas = data.response.data;

    //sort api to firstname
    this.dataTest = JSON.parse(JSON.stringify(this.datas));
    this.dataFinal =  this.dataTest.sort(function(a, b) {
        return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
    });
    this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId
    console.log(this.compeId);

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/standings?ak=eebd8ae256142ac3fd24bd2003d28782')
    .map(res => res.json())
    .subscribe(data => {
      this.teamStandings = data.response.data;  


    console.log(this.ulan, 'test ulan win');

      //  //sort api to position
      //  this.stands = JSON.parse(JSON.stringify(this.teamStandings));
      //  this.teamFinalStandings =  this.stands.sort(function(a, b) {
      //      return parseInt(b.percentageWon) - parseInt(a.percentageWon);
      //  });

       this.stands = JSON.parse(JSON.stringify(this.teamStandings));
       this.teamFinalStandings =  this.stands.sort(function(a, b) {
           return (parseFloat(b.percentageWon) < parseFloat(a.percentageWon)) ? -1 : (parseFloat(b.percentageWon) > parseFloat(a.percentageWon)) ? 1 : 0;
       });

       this.loading.dismiss();
      console.log(this.teamFinalStandings,'test standing');
    }, err => {
      //this.presentConfirm();
      console.log(err); 
    });
})
.catch( error => {
  console.log('error');
  this.presentConfirm();  
})
}


  //standing win percentage 
  winpercentage(teamFinalStanding) {

    if(teamFinalStanding.played > 0 ) {
      if(teamFinalStanding.won == 0) {
          
          return '.000';
          
      } else {
          
          let totalHa = teamFinalStanding.won / teamFinalStanding.played;
          let totalHaString = totalHa.toString();
          let totalHaTest = totalHaString.length;
          
          if(totalHaTest == 1) {
              return '100';
          } else {
              
              let shitHa = totalHa.toString();
              let remove = shitHa.substring(1, 5);
              let yeah = remove.length;
              
              if(yeah == 2) {

                  return remove + '00';
                  
              } 
              
              if(yeah == 3) {

                  return remove + '0';

              }

              if(yeah == 4) {

                return remove;

              }

              
        
          
          }
          
      }
      
  } else {
      
      return '.000';
      
  }

}



  //standing l5
  lastpayb(teamFinalStanding) {
    let a = teamFinalStanding.last5;
    let w = a.split("W").length - 1;
    let l = a.split("L").length - 1;
    return w + '-' + l; 
  }

  //standing streak
  streak(teamFinalStanding) {
    let a = teamFinalStanding.last5;
    let lastChar = a.substr(a.length - 1);
    if(teamFinalStanding.streak.length > 1) {
      let res = teamFinalStanding.streak.slice(1, 3);
      return lastChar + res;
    } else {
      return lastChar + teamFinalStanding.streak;
    }
  }

  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.latestCompetitionId();
  }

  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

}
