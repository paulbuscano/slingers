import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , Slides , Content,Platform} from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import { DatePipe } from '@angular/common';

@IonicPage()
@Component({
  selector: 'page-sample',
  templateUrl: 'sample.html',
})
export class SamplePage {

  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  @ViewChild('MultiItemsScrollingTabs') ItemsTitles : Content;

  SwipedTabsIndicator :any= null;
  tabTitleWidthArray :any= [];
  tabElementWidth_px :number= 50;
  screenWidth_px :number= 0;
  isRight :boolean= true;
  isLeft:boolean= true;
  tabs:any=[];

  matches : any;

  dataMatches : any;

  collection: any=[];
  shits:any=[];

  constructor(public http: Http,public navCtrl: NavController ,platform: Platform) {
    
    this.tabs=["FRI, 17 APR","SUN, 52 APR","SUN, 26 APR","THU, 27 APR","THU, 89 APR","THU, 19 APR","THU, 10 APR","THU, 11 APR","THU, 12 APR","THU, 13 APR","THU, 14 SEP","THU, 54 SEP"];
    this.getMatches();
    console.log('Width: ' + platform.width());
    this.screenWidth_px=platform.width();

  }

     //get genius matches with dismiss loading gif
     getMatches(){
  
      this.http.get('https://api.wh.geniussports.com/v1/basketball/competitions/19816/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&teamId=88261&limit=11')
      .map(res => res.json())
      .subscribe(data => {
        this.dataMatches = data.response.data;
        console.log(this.dataMatches);
  
        for(let i = 0; i < this.dataMatches.length; i++) {

          this.collection[i] = this.dataMatches[i].matchTime;
          var datePipe = new DatePipe("en-US");
          this.collection[i] = datePipe.transform(this.collection[i], 'EEE, dd MMM');
          this.shits[i] = this.dataMatches[i];
        }
        this.tabs = this.collection;
        console.log(this.tabs);
      }, err => {
        console.log(err);
      
      });
    }

  ionViewDidEnter() {
    this.SwipedTabsIndicator = document.getElementById("indicator");
    for (let i in this.tabs)
      this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth);

    this.selectTab(4);
  }

  scrollIndicatiorTab()
  {
    this.ItemsTitles.scrollTo(this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())-this.screenWidth_px/3,50);
  }

  selectTab(index)
  {
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';
    this.SwipedTabsSlider.slideTo(index);
  }

  calculateDistanceToSpnd(index)
  {
    var result=0;
    for (var _i = 0; _i < index; _i++) {
      result=result+this.tabTitleWidthArray[_i];
    }
    return result;
  }

  updateIndicatorPosition() {
    var index=this.SwipedTabsSlider.getActiveIndex();
    if( this.SwipedTabsSlider.length()==index)
      index=index-1;

    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';

  }

  updateIndicatorPositionOnTouchEnd()
  {
    setTimeout( () => { this.updateIndicatorPosition(); }, 200);
  }

  animateIndicator($event)
  {

    this.isLeft=false;
    this.isRight=false;
    var currentSliderCenterProgress =(1/(this.SwipedTabsSlider.length()-1) )*this.SwipedTabsSlider.getActiveIndex();
    if($event.progress < currentSliderCenterProgress)
    {
      this.isLeft=true;
      this.isRight=false;

    } if($event.progress > currentSliderCenterProgress)
    {
      this.isLeft=false;
      this.isRight=true;
    }

    if(this.SwipedTabsSlider.isEnd())
      this.isRight=false;

    if( this.SwipedTabsSlider.isBeginning())
      this.isLeft=false;

    if(this.isRight)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()+1])
      +'px,0,0)';

    if(this.isLeft)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()-1])
      +'px,0,0)';

    if(!this.isRight && !this.isLeft)
      this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()]+"px";

  }

}
