import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController, Platform } from 'ionic-angular';
import { ViewController, Content } from 'ionic-angular';
import { PlaybyplayPage } from '../playbyplay/playbyplay';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-game-detail',
  templateUrl: 'game-detail.html',
})

export class GameDetailPage {

  @ViewChild(Content) content: Content;
  
  fuck: any;
  lets = "/v1";
  dataMatchesOne : any;
  dataMatchesTwo : any;
  loading : any;
  pota : any;
  gago : any;
  citrus : any=[];
  lonely : any;
  citrusTwo : any=[];
  lonelyTwo : any;
  match : any;

  rockets : any=[];
  rocketsTwo : any=[];
  potanas : any;
  segments : any;
  teams : any;
  dataLogs : any=[];
  patas : any;
  heads : any=[];
  headsTwo : any=[];
  boxes : any;
  starters : any=[];
  benches: any=[];

  boxesTwo : any;
  startersTwo : any=[];
  benchesTwo: any=[];
  dataLeaders : any;
  leadPoints : any;
  leadRebounds : any;
  leadAsssists : any;

  dataLeadersPoints : any;
  dataLeadersAssists : any;
  dataLeadersRebounds : any;

  points : any;
  assists : any;
  rebounds : any;

  pointsTwo : any;
  assistsTwo : any;
  reboundsTwo : any;

  dataLeadersTwo : any;
  leadPointsTwo : any;
  leadReboundsTwo : any;
  leadAsssistsTwo : any;

  page = 0;
  maximumPages = 1000;
  periodOnes : any=[];
  periodTwos : any=[];
  periodThrees : any=[];
  periodFours : any=[];
  periodOtOnes : any=[];
  periodOtTwos : any=[];
  safari : any;
  periodsOnesTest: any;
  periodOnesNew: any=[];
  periodsTwosTest: any;
  periodTwosNew: any=[];
  periodsThreesTest: any;
  periodThreesNew: any=[];
  periodsFoursTest: any;
  periodFoursNew: any=[];
  periodsOtOnesTest: any;
  periodOtOnesNew: any=[];
  periodsOtTwosTest: any;
  periodOtTwosNew: any=[];
  shitOTTwo : any;
  shitOTOne : any;
  quarter : any;
  dataLive: any=[];
  films: Observable<any>;
  url : any;
  
  constructor(private platform: Platform, private alertCtrl: AlertController, private LoadingCtrl: LoadingController, public http: Http, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.segments = "overview";
    this.teams = "away";
    this.safari = "One";
    this.fuck = navParams.get('post');
    this.pota = navParams.get('teamOne');
    this.gago = navParams.get('teamTwo');
    this.match = navParams.get('match');
    this.presentLoader();
    this.getPlayByPlay();
    this.getHeadtoHeadOne();
    this.getHeadtoHeadTwo();
    this.getBoxscoreOne();
    this.getBoxscoreTwo();
    console.log(this.fuck, 'kaya');

    this.url = "http://www.fibalivestats.com/u/abl/"+ this.fuck.matchId;
    console.log(this.url);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GameDetailPage');
    this.viewCtrl.setBackButtonText('');
    this.getLive();
  }

  scrollTo(elementId: string) {
    //let y = document.getElementById(elementId).offsetTop;
    this.content.scrollTo(0, 0);
    //let x = document.getElementById("busy").style.backgroundColor = "#eaeaea";
    //let z = document.getElementById("busy2").style.backgroundColor = "#eaeaea";
  }

  //test live
  getLive(){ 
      // this.http.get('https://live.wh.geniussports.com/v2/basketball/read/987169?ak=eebd8ae256142ac3fd24bd2003d28782')
      // .subscribe(data => {
      //   this.dataLive = data;
      //   console.log(this.dataLive, 'test live data');
      // }, err => {
      //   console.log(err); 
      // });

  
   
  }

  //get genius matches team one with leaders 
  getLeadersMatchOne(){ 

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/statistics/personmatch/matches/'+this.match+'/teams/'+this.pota+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=30')
    .map(res => res.json())
    .subscribe(data => {
      this.dataLeaders = data.response.data;  
     

      //sort api to points
      this.points = JSON.parse(JSON.stringify(this.dataLeaders));
      let pointsL =  this.points.sort(function(a, b) {
          return parseInt(b.sPoints) - parseInt(a.sPoints);
      });
      
      //sort api to assists
      this.assists = JSON.parse(JSON.stringify(this.dataLeaders));
      let assistsL =  this.assists.sort(function(a, b) {
          return parseInt(b.sAssists) - parseInt(a.sAssists);
      });

      //sort api to rebounds
      this.rebounds = JSON.parse(JSON.stringify(this.dataLeaders));
      let reboundsL =  this.rebounds.sort(function(a, b) {
          return parseInt(b.sReboundsTotal) - parseInt(a.sReboundsTotal);
      });

      this.leadPoints = pointsL.slice(0, 1); //get first array high points
      this.leadAsssists = assistsL.slice(0, 1); //get first array high assists
      this.leadRebounds = reboundsL.slice(0, 1); //get first array high rebounds
      console.log('test');
      console.log(this.leadPoints);
      console.log(this.leadAsssists);
      console.log(this.leadRebounds);
    }, err => {
      console.log(err); 
    });
  }

  //get genius matches team two with leaders 
  getLeadersMatchTwo(){ 

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/statistics/personmatch/matches/'+this.match+'/teams/'+this.gago+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=30')
    .map(res => res.json())
    .subscribe(data => {
      this.dataLeadersTwo = data.response.data;  
     

      //sort api to points
      this.pointsTwo = JSON.parse(JSON.stringify(this.dataLeadersTwo));
      let pointsL =  this.pointsTwo.sort(function(a, b) {
          return parseInt(b.sPoints) - parseInt(a.sPoints);
      });
      
      //sort api to assists
      this.assistsTwo = JSON.parse(JSON.stringify(this.dataLeadersTwo));
      let assistsL =  this.assistsTwo.sort(function(a, b) {
          return parseInt(b.sAssists) - parseInt(a.sAssists);
      });

      //sort api to rebounds
      this.reboundsTwo = JSON.parse(JSON.stringify(this.dataLeadersTwo));
      let reboundsL =  this.reboundsTwo.sort(function(a, b) {
          return parseInt(b.sReboundsTotal) - parseInt(a.sReboundsTotal);
      });

      this.leadPointsTwo = pointsL.slice(0, 1); //get first array high points
      this.leadAsssistsTwo = assistsL.slice(0, 1); //get first array high assists
      this.leadReboundsTwo = reboundsL.slice(0, 1); //get first array high rebounds
      console.log('test 2');
      console.log(this.leadPointsTwo);
      console.log(this.leadAsssistsTwo);
      console.log(this.leadReboundsTwo);
    }, err => {
      console.log(err); 
    });
  }

  //player last name convert 
    leadersLastname(leadPoint) {
    let get = leadPoint.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

  //player last name convert two 
  leadersLastnameTwo(leadPointTw) {
    let get = leadPointTw.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

   //player last name convert 
   leadersLastnameLeaders(leadRebound) {
    let get = leadRebound.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

  //player last name convert two 
  leadersLastnameLeadersTwo(leadReboundsTw) {
    let get = leadReboundsTw.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

   //player last name convert 
   leadersLastnameLeadersAssists(leadAsssist) {
    let get = leadAsssist.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

  //player last name convert two 
  leadersLastnameLeadersAssistsTwo(leadAsssistsTw) {
    let get = leadAsssistsTw.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
    
    return upper;
  }

  //get genius matches with dismiss loading gif
  getPlayByPlay(){ 

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/matches/'+this.match+'/actions?ak=eebd8ae256142ac3fd24bd2003d28782&scoring=1&limit=1000')
    .map(res => res.json())
    .subscribe(data => {
      //this.dataLogs = data.response.data; 
      this.dataLogs = data.response.data;
      console.log('potana'); 
      console.log(this.dataLogs);

      for(let i = 0; i < this.dataLogs.length; i++) {
          if (this.dataLogs[i].period === 1 && this.dataLogs[i].periodType === "REGULAR" ) {
            this.periodOnesNew.push(this.dataLogs[i]);
        } else if(this.dataLogs[i].period === 2 && this.dataLogs[i].periodType === "REGULAR") {
          this.periodTwosNew.push(this.dataLogs[i]);
        }
        else if(this.dataLogs[i].period === 3 && this.dataLogs[i].periodType === "REGULAR") {
          this.periodThreesNew.push(this.dataLogs[i]);
        }
        else if(this.dataLogs[i].period === 4 && this.dataLogs[i].periodType === "REGULAR") {
          this.periodFoursNew.push(this.dataLogs[i]);
        }
        else if(this.dataLogs[i].period === 1 && this.dataLogs[i].periodType === "OVERTIME") {
          this.periodOtOnesNew.push(this.dataLogs[i]);
        }
        else if(this.dataLogs[i].period === 2 && this.dataLogs[i].periodType === "OVERTIME") {
          this.periodOtTwosNew.push(this.dataLogs[i]);
        }
      }
        
      //sort new array period 1 
      this.periodsOnesTest = JSON.parse(JSON.stringify(this.periodOnesNew));
      this.periodOnes =  this.periodsOnesTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });

      //sort new array period 2
      this.periodsTwosTest = JSON.parse(JSON.stringify(this.periodTwosNew));
      this.periodTwos =  this.periodsTwosTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });

      //sort new array period 3
      this.periodsThreesTest = JSON.parse(JSON.stringify(this.periodThreesNew));
      this.periodThrees =  this.periodsThreesTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });

      //sort new array period 4
      this.periodsFoursTest = JSON.parse(JSON.stringify(this.periodFoursNew));
      this.periodFours =  this.periodsFoursTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });

      //sort new array period 1 OT
      this.periodsOtOnesTest = JSON.parse(JSON.stringify(this.periodOtOnesNew));
      this.periodOtOnes =  this.periodsOtOnesTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });

      //sort new array period 2 OT
      this.periodsOtTwosTest = JSON.parse(JSON.stringify(this.periodOtTwosNew));
      this.periodOtTwos =  this.periodsOtTwosTest.sort(function(a, b) {
          return parseInt(a.actionNumber) - parseInt(b.actionNumber);
      });
      
      this.shitOTTwo = this.periodOtTwos.length;  
      this.shitOTOne = this.periodOtOnes.length;
      this.loading.dismiss();

      console.log('periodOnes'); 
      console.log(this.periodOnes);

    }, err => {
      this.loading.dismiss();
      console.log(err); 
    });
  }


  //get genius matches with dismiss loading gif
  getStatsOne(){ 

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/statistics/teammatch/matches/'+this.match+'/teams/'+this.pota+'?ak=eebd8ae256142ac3fd24bd2003d28782')
    .map(res => res.json())
    .subscribe(data => {
      this.dataMatchesOne = data.response.data;  

      this.potanas = this.dataMatchesOne.slice(1, this.dataMatchesOne.length); //all score including over time

      this.lonely = this.dataMatchesOne.slice(0, 1);      //get first array last score
 
      this.citrus = this.dataMatchesOne.slice(1, this.dataMatchesOne.length); //all score including over time

      for(let i = 0; i < this.citrus.length; i++) {
        if (this.citrus[i].periodType === "OVERTIME") {
          this.rockets[i] = this.citrus[i];
          this.citrus.splice(i, 1);
      }
      }
     // console.log(this.rockets);
      //console.log(this.citrus);
       
    }, err => {
      
      console.log(err); 
    });
  }

   //get genius matches with dismiss loading gif
   getStatsTwo(){
  
    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/statistics/teammatch/matches/'+this.match+'/teams/'+this.gago+'?ak=eebd8ae256142ac3fd24bd2003d28782')
    .map(res => res.json())
    .subscribe(data => {
      this.dataMatchesTwo = data.response.data;  

      this.lonelyTwo = this.dataMatchesTwo.slice(0, 1);      //get first array last score

      this.citrusTwo = this.dataMatchesTwo.slice(1, this.dataMatchesTwo.length); //all score including over time
      
      // determine if has overtime score
      for(let i = 0; i < this.citrusTwo.length; i++) {
        if (this.citrusTwo[i].periodType === "OVERTIME") {
          this.rocketsTwo[i] = this.citrusTwo[i];
          this.citrusTwo.splice(i, 1);
      }
      }
     //console.log(this.rocketsTwo);
     //console.log(this.citrusTwo);
  
    }, err => {
      
      console.log(err); 
    });
  }

  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.getStatsTwo();
    this.getStatsOne();
    this.getLeadersMatchOne();
    this.getLeadersMatchTwo();
    this.getPlayByPlay();
  }

   //alert if no internet
   presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  playByplay() {
    this.navCtrl.push(PlaybyplayPage);
  }

  currentTime(dataLog) {
    let str = dataLog.clock;
    let res = str.substr(0, 5);
    return res;
  }

    //get head to head team one
    getHeadtoHeadOne() { 
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/statistics/teammatch/matches/'+this.match+'/teams/'+this.pota+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0')
      .map(res => res.json())
      .subscribe(data => {
        this.heads = data.response.data;  

      //console.log(this.heads);
    
      }, err => {
        
        console.log(err); 
      });
    }

    //get head to head team two
    getHeadtoHeadTwo() { 
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/statistics/teammatch/matches/'+this.match+'/teams/'+this.gago+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0')
      .map(res => res.json())
      .subscribe(data => {
        this.headsTwo = data.response.data;  

      //console.log(this.headsTwo);
    
      }, err => {
        
        console.log(err); 
      });
    }

     //get box score team one
     getBoxscoreOne() { 
      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/statistics/personmatch/matches/'+this.match+'/teams/'+this.pota+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=80')
      .map(res => res.json())
      .subscribe(data => {
        this.boxes = data.response.data;  
        console.log(this.boxes);

      // determine if starter or not
      for(let i = 0; i < this.boxes.length; i++) {
        if (this.boxes[i].isStarter == 1) {
          this.starters.push(this.boxes[i]);
        } else {
          this.benches.push(this.boxes[i]);
        }
      }
      console.log(this.starters, 'test starters');
      console.log(this.benches);
     
      }, err => {
        
        console.log(err); 
      });
    }

      //get box score team one
      getBoxscoreTwo() { 
        if (this.platform.is('mobile')) {
          this.lets = "https://api.wh.geniussports.com/v1";
        } else {
          this.lets = "/v1";
        }
  
        this.http.get(this.lets + '/basketball/statistics/personmatch/matches/'+this.match+'/teams/'+this.gago+'?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=80')
        .map(res => res.json())
        .subscribe(data => {
          this.boxesTwo = data.response.data;  
          console.log(this.boxesTwo);
  
        // determine if starter or not
        for(let i = 0; i < this.boxesTwo.length; i++) {
          if (this.boxesTwo[i].isStarter == 1) {
            this.startersTwo.push(this.boxesTwo[i]);
          } else {
            this.benchesTwo.push(this.boxesTwo[i]);
          }
        }
        console.log(this.startersTwo);
        console.log(this.benchesTwo);
       
        }, err => {
          
          console.log(err); 
        });
      }

    //convert percentage field goal
    percentageFg(head) {
      let babago = head.sFieldGoalsPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    percentageFgTwo(headsTw) {
      let babago = headsTw.sFieldGoalsPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    //convert percentage three point field goal
    percentageThreefg(head) {
      let babago = head.sThreePointersPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    percentageThreefgTwo(headsTw) {
      let babago = headsTw.sThreePointersPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

     //convert percentage two point field goal
     percentageTwofg(head) {
      let babago = head.sTwoPointersPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    percentageTwofgTwo(headsTw) {
      let babago = headsTw.sTwoPointersPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

     //convert percentage free throw
     percentageFt(head) {
      let babago = head.sFreeThrowsPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    percentageFtTwo(headsTw) {
      let babago = headsTw.sFreeThrowsPercentage;
      var n = babago.toString();
      let result = n.slice(2);
      if(result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }

    //player first name convert 
    firstnameOne(starter) {
      let kuha = starter.firstName;
      let f = kuha.toString();
      let n = f.substr(0, 1);
      
      return n + '.';
    }

    //player first name convert 
    firstnamePbpOne(periodOne) {
      let kuha = periodOne.firstName;
      let n = kuha[0];
      
      return n + '.';
    }

    //player first name convert 
    firstnamePbpTwo(periodTwo) {
      let kuha = periodTwo.firstName;
      let n = kuha[0];
      
      return n + '.';
    }

    //player first name convert 
    firstnamePbpThree(periodThree) {
    let kuha = periodThree.firstName;
    let n = kuha[0];
    
      return n + '.';
    } 

    //player first name convert 
    firstnamePbpFour(periodFour) {
      let kuha = periodFour.firstName;
      let n = kuha[0];
      
      return n + '.';
    } 

    //player first name convert 
    firstnamePbpOtOne(periodOtOne) {
      let kuha = periodOtOne.firstName;
      let n = kuha[0];
      
      return n + '.';
    }

    //player first name convert 
    firstnamePbpOtTwo(periodOtTwo) {
      let kuha = periodOtTwo.firstName;
      let n = kuha[0];
      
      return n + '.';
    }

    //player last name convert 
    lastnameOne(starter) {
      let get = starter.familyName;
      let g = get.toString();
      let pota = get.toLowerCase();
      let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
      
      return upper;
    }

    //player minutes convert
    minutesOne(starter) {
      let getmin = starter.sMinutes;
      let n = getmin.toString();
      if(n.length > 3){
        if(n.length == 4) {
          let bala = n.slice(0, 1);
          return bala;
        } else {
          let result = n.slice(0, 2);
          return result;
        }
      } else {
        return n;
      }
    }

    //player fg convert
    fieldgoalOne(starter) {
      let fg =  starter.sFieldGoalsPercentage;
      let n = fg.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

    //player three points convert
    threepointsOne(starter) {
      let tp =  starter.sThreePointersPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

     //player three points convert
     freethrowsOne(starter) {
      let tp =  starter.sFreeThrowsPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

    //player first name convert bench
    firstnamebenchOne(benche) {
      let kuha = benche.firstName;
      let f = kuha.toString();
      let n = f.substr(0, 1);
      
      return n + '.';
    }


    //player last name convert bench
    lastnamebenchOne(benche) {
      let get = benche.familyName;
      let g = get.toString();
      let pota = get.toLowerCase();
      let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
      
      return upper;
    }

      //player minutes convert bench
      minutesbenchOne(benche) {
        let getmin = benche.sMinutes;
        let n = getmin.toString();
        if(n.length > 3){
          if(n.length == 4) {
            let bala = n.slice(0, 1);
            return bala;
          } else {
            let result = n.slice(0, 2);
            return result;
          }
        } else {
          return n;
        }
   
      }
  
      //player fg convert bench
      fieldgoalbenchOne(benche) {
        let fg =  benche.sFieldGoalsPercentage;
        let n = fg.toString();
        if(n.length < 2) {
            if(n == 1) {
              return n + '00';
            } else {
              return n;
            }
        } else {
          let result = n.slice(2, 4);
          if(result.length < 2) {
            return result + '0';
          } else {
            return result;
          }
        }
      }
  
      //player three points convert bench
      threepointsbenchOne(benche) {
        let tp =  benche.sThreePointersPercentage;
        let n = tp.toString();
        if(n.length < 2) {
            if(n == 1) {
              return n + '00';
            } else {
              return n;
            }
        } else {
          let result = n.slice(2, 4);
          if(result.length < 2) {
            return result + '0';
          } else {
            return result;
          }
        }
      }

         //player three points convert
     freethrowsbenchOne(benche) {
      let tp =  benche.sFreeThrowsPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

    //player minutes convert
    minutesTwo(starterTw) {
      let getmin = starterTw.sMinutes;
      let n = getmin.toString();
      if(n.length > 3){
        if(n.length == 4) {
          let bala = n.slice(0, 1);
          return bala;
        } else {
          let result = n.slice(0, 2);
          return result;
        }
      } else {
        return n;
      }
    }

    //player fg convert
    fieldgoalTwo(starterTw) {
      let fg =  starterTw.sFieldGoalsPercentage;
      let n = fg.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

    //player three points convert
    threepointsTwo(starterTw) {
      let tp =  starterTw.sThreePointersPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

     //player three points convert
     freethrowsTwo(starterTw) {
      let tp =  starterTw.sFreeThrowsPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }

      //player first name convert 
      firstnameTwo(startersTw) {
        let kuha = startersTw.firstName;
        let f = kuha.toString();
        let n = f.substr(0, 1);
        
        return n + '.';
      }
  
  
      //player last name convert 
      lastnameTwo(startersTw) {
        let get = startersTw.familyName;
        let g = get.toString();
        let pota = get.toLowerCase();
        let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
        
        return upper;
      }

         //player first name convert bench
    firstnamebenchTwo(bencheTw) {
      let kuha = bencheTw.firstName;
      let f = kuha.toString();
      let n = f.substr(0, 1);
      
      return n + '.';
    }


    //player last name convert bench
    lastnamebenchTwo(bencheTw) {
      let get = bencheTw.familyName;
      let g = get.toString();
      let pota = get.toLowerCase();
      let upper = pota.charAt(0).toUpperCase() + pota.substr(1);
      
      return upper;
    }

      //player minutes convert bench
      minutesbenchTwo(bencheTw) {
        let getmin = bencheTw.sMinutes;
        let n = getmin.toString();
        if(n.length > 3){
          if(n.length == 4) {
            let bala = n.slice(0, 1);
            return bala;
          } else {
            let result = n.slice(0, 2);
            return result;
          }
        } else {
          return n;
        }
   
      }
  
      //player fg convert bench
      fieldgoalbenchTwo(bencheTw) {
        let fg =  bencheTw.sFieldGoalsPercentage;
        let n = fg.toString();
        if(n.length < 2) {
            if(n == 1) {
              return n + '00';
            } else {
              return n;
            }
        } else {
          let result = n.slice(2, 4);
          if(result.length < 2) {
            return result + '0';
          } else {
            return result;
          }
        }
      }
  
      //player three points convert bench
      threepointsbenchTwo(bencheTw) {
        let tp =  bencheTw.sThreePointersPercentage;
        let n = tp.toString();
        if(n.length < 2) {
            if(n == 1) {
              return n + '00';
            } else {
              return n;
            }
        } else {
          let result = n.slice(2, 4);
          if(result.length < 2) {
            return result + '0';
          } else {
            return result;
          }
        }
      }

         //player three points convert
     freethrowsbenchTwo(bencheTw) {
      let tp =  bencheTw.sFreeThrowsPercentage;
      let n = tp.toString();
      if(n.length < 2) {
          if(n == 1) {
            return n + '00';
          } else {
            return n;
          }
      } else {
        let result = n.slice(2, 4);
        if(result.length < 2) {
          return result + '0';
        } else {
          return result;
        }
      }
    }
    liveClock(fuck) {
      let periodType = fuck.live.periodType;
        if(periodType == 'REGULAR') {
            this.quarter = 'Q' + fuck.live.period;
        } else {
            this.quarter = 'OT' + fuck.live.period;
        }
      let time = fuck.live.clock;
      let trimTime = time.split(':');
      return this.quarter + ' ' + trimTime[0] + ':' + trimTime[1];
    } 
}
