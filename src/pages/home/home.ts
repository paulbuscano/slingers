import { Component, Testability } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController, Platform } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';
import { DetailNewsPage } from '../detail-news/detail-news';
import { Network } from '@ionic-native/network';
import { GameDetailPage } from '../game-detail/game-detail';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ModalPage } from '../modal/modal';
import { StatusBar } from '@ionic-native/status-bar';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  data : any;

  datas : any;

  matches : any;

  loading : any;

  dataMatches : any;

  public per_page: number = 5;
  public page: number = 1;

  lets = "/v1";
  dataTest : any;
  dataFinal : any;
  compeId : any;
  datasCompe : any;
  teamId = '79';
  dateTest: any;
  dateNow : any;
  dateTestNow: any;
  month: any;
  test: any;
  dataMatchesShit: any;
  dataMatchesTest: any;
  dataMatchesEy: any;

  constructor(statusBar: StatusBar, private socialSharing: SocialSharing, public http: Http,private platform: Platform, public navCtrl: NavController, private LoadingCtrl: LoadingController, public api: ApiProvider, private alertCtrl: AlertController, private network: Network) {
    this.presentLoader();
  }

  //pass data to game details page
  gameDetails(dataMatch) {
    this.navCtrl.push(GameDetailPage, { post:dataMatch, teamOne:dataMatch.competitors[0].competitorId, teamTwo:dataMatch.competitors[1].competitorId, match:dataMatch.matchId });
  }

  //pass data from details news page
  openNews(data,i) {
    this.navCtrl.push(DetailNewsPage, { post:data, test:i });
  }
  

  share(data) {
    this.socialSharing.share(data.link)
    .then(()=> {

    }).catch(()=> {

    });
  }

  openYoutube() {
    this.navCtrl.push(ModalPage);
  }


  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.getPosts();
    this.getMatches();
  }

  //get post with dismiss loading gif
  getPosts(){
    this.api.load()
    .then( data => {
      this.datas = data;
      this.loading.dismiss();

      console.log(this.datas);
    })
    .catch( error => {
      console.log('error');
      this.presentConfirm();

     
    })
  }

  //get genius matches with dismiss loading gif
getMatches(){

  this.api.loadLatestCompetitionId()
  .then( data => {
    this.datasCompe = data.response.data;
  
    //sort api to firstname
    this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
    this.dataFinal =  this.dataTest.sort(function(a, b) {
        return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
    });
    this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    var date = new Date();

    var year = date.getFullYear();
    this.month = date.getMonth()+1;
    var day = date.getDate();
    
    if (day < 10) {
       let dayz = '0' + day;
    }
    if (this.month < 10) {
      let monthz = '0' + this.month;
    }
    
    this.dateTestNow = year + '-' + this.month + '-' + day


    this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&fromDate='+ this.dateTestNow +'&teamId='+ this.teamId +'&limit=150')
    .map(res => res.json())
    .subscribe(data => {

      this.dataMatchesShit = data.response.data;

      //sort schedule by asc
      this.dataMatchesTest = JSON.parse(JSON.stringify(this.dataMatchesShit));
      this.dataMatchesEy =  this.dataMatchesTest.sort(function(a, b) {
          return (a.matchTime < b.matchTime) ? -1 : (a.matchTime > b.matchTime) ? 1 : 0;
      });

      this.dataMatches = this.dataMatchesEy.slice(0, 3);
      console.log(this.dataMatches , 'alright');
    }, err => {
      console.log(err);
      this.presentConfirm();
    });
  })
  .catch( error => {
    console.log('error');
    this.presentConfirm();  
  })
}


  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  //pull to refresh 
  doRefresh(refresher) {
     setTimeout(() => {
        console.log('Async operation has ended');
        this.getPosts();
        this.getMatches();
        refresher.complete();
      }, 2000);
  }

  //change venue name to all caps
  venue(dataMatch) {
    let venue = dataMatch.venue.venueName;
    let res = venue.toUpperCase();
    return res;
  }

  

}