import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { ModalPage } from '../modal/modal';
import { ApiProvider } from '../../providers/api/api';
import { AppAvailability } from '@ionic-native/app-availability';

@IonicPage()
@Component({
  selector: 'page-videos',
  templateUrl: 'videos.html',
})
export class VideosPage {

  datas : any;

  loading : any;
  test : any;
  hasYoutube: any;
  dataTest: any;
  dataFinal: any;
  datasFinals : any;

  constructor(private platform: Platform, private appAvailability: AppAvailability, private LoadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, private modal: ModalController, public api: ApiProvider, private alertCtrl: AlertController) {
    this.presentLoader();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VideosPage');
  }

  //get youtube videos from channel
  getVideos() {
    this.api.loadYoutube()
    .then(data => {
      this.datas = data.items;

        //sort api to date
        this.dataTest = JSON.parse(JSON.stringify(this.datas));
        this.dataFinal = this.dataTest.sort(function (a, b) {
          return (b.snippet.publishedAt < a.snippet.publishedAt) ? -1 : (b.snippet.publishedAt > a.snippet.publishedAt) ? 1 : 0;
        });
        this.datasFinals = this.dataFinal;
      console.log(this.datasFinals, 'test video');

      this.loading.dismiss();
      console.log(this.datas);
    })
    .catch( error => {
      console.log('error');
      this.presentConfirm();
    })
  }

  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.getVideos();
  }

  //pull to refresh 
  doRefresh(refresher) {
    setTimeout(() => {
        console.log('Async operation has ended');
        this.getVideos();
        refresher.complete();
      }, 2000);
  }

  youtube(data){
    let app;
    let facebookId = data.snippet.resourceId.videoId;
    if (this.platform.is('ios')) {
      app = 'youtube://';
    } else if (this.platform.is('android')) {
      app = 'com.youtube.android';
    }

    this.appAvailability.check(app)
      .then(
      (yes: boolean) => this.hasYoutube ='youtube://'+ facebookId +'',
     
      (no: boolean) => this.hasYoutube = 'https://www.youtube.com/watch?v='+ facebookId +''
      );

      window.open(this.hasYoutube, '_system', 'location=yes'); return false;
  }

 

}
