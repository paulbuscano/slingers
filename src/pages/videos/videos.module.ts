import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VideosPage } from './videos';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(VideosPage),
  ],
})
export class VideosPageModule {}
