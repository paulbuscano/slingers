import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GamesPage } from './games';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(GamesPage),
  ],
})
export class GamesPageModule {}
