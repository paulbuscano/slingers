import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSocialPage } from './modal-social';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ModalSocialPage),
  ],
})
export class ModalSocialPageModule {}
