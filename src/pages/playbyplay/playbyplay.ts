import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { GameDetailPage } from '../game-detail/game-detail';
import { ViewController } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@IonicPage()
@Component({
  selector: 'page-playbyplay',
  templateUrl: 'playbyplay.html',
})
export class PlaybyplayPage {

  lets : any;
  dataLogs : any;
  private _data = [
    'Fast Times at Ridgemont High',
    'Peggy Sue Got Married',
    'Raising Arizona',
    'Moonstruck',
    'Fire Birds',
    'Honeymoon in Vegas',
    'Amos & Andrew',
    'It Could Happen to You',
    'Trapped in Paradise',
    'Leaving Las Vegas',
    'The Rock',
    'Con Air',
    'Face/Off',
    'City of Angels',
    'Gone in Sixty Seconds',
    'The Family Man',
    'Windtalkers',
    'Matchstick Men',
    'National Treasure',
    'Ghost Rider',
    'Grindhouse',
    'Next',
    'Kick-Ass',
    'Drive Angry',
  ];
  items = [];

  constructor(public http: Http, private platform: Platform, public navCtrl: NavController, public navParams: NavParams , public viewCtrl: ViewController) {
    this.fuck();
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlaybyplayPage');
    this.viewCtrl.setBackButtonText('');
  }

  overview() {
    this.navCtrl.push(GameDetailPage);
  }

  fuck(infiniteScroll?) {
    for (let i = 0; i < this._data.length; i++) {
      this.items.push( this._data[i] );
    }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      for (let i = 1; i < 30; i++) {
        this.items.push( this._data[i] );
      }

      console.log('Async operation has ended');
      infiniteScroll.complete();
      if (this.items.length > this._data.length) {
        infiniteScroll.enable(false);
      }
    }, 500);

   
  }
 

}
