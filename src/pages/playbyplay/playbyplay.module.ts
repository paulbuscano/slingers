import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaybyplayPage } from './playbyplay';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(PlaybyplayPage),
  ],
})
export class PlaybyplayPageModule {}
