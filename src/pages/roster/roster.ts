import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, AlertController, Platform } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';
import { PlayerDetailPage } from '../player-detail/player-detail';

@IonicPage()
@Component({
  selector: 'page-roster',
  templateUrl: 'roster.html',
})
export class RosterPage {

  lets : any;
  players : any;
  teamId = '79'; 
  pass : any;
  loading : any;
  segments : any;
  playersNames : any;
  sortNames : any;
  playersPosition: any;
  sortPositions : any;
  compeId: any;
  datasCompe: any;
  dataTest: any;
  dataFinal: any;
  playersNumbers: any;
  sortNumbers : any;

  constructor(public api: ApiProvider,private LoadingCtrl: LoadingController, private alertCtrl: AlertController , public http: Http,private platform: Platform, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.segments = "number"; //default segment
    this.presentLoader();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RosterPage');
    this.viewCtrl.setBackButtonText('');
  }

    //get genius by click id
    getPlayers(){ 
      this.api.loadLatestCompetitionId()
      .then( data => {
        this.datasCompe = data.response.data;
      
        //sort api to firstname
        this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
        this.dataFinal =  this.dataTest.sort(function(a, b) {
            return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
        });
        this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
        //console.log(this.compeId);

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }
  
      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/teams/'+ this.teamId +'/persons?ak=eebd8ae256142ac3fd24bd2003d28782&limit=30')
      .map(res => res.json())
      .subscribe(data => {
        this.players = data.response.data;  

        //sort api to number

        this.playersNumbers = JSON.parse(JSON.stringify(this.players));
        let pointsN =  this.playersNumbers.sort(function(a, b) {
            return (parseInt(a.competition_player.shirtNumber, 10) < parseInt(b.competition_player.shirtNumber)) ? -1 : (parseInt(a.competition_player.shirtNumber) > parseInt(b.competition_player.shirtNumber)) ? 1 : 0;
        });

         //sort api to firstname
        this.playersNames = JSON.parse(JSON.stringify(this.players));
        let pointsL =  this.playersNames.sort(function(a, b) {
            return (a.familyName < b.familyName) ? -1 : (a.familyName > b.familyName) ? 1 : 0;
        });

        //sort api to position
        this.playersPosition = JSON.parse(JSON.stringify(this.players));
        let positionL =  this.playersPosition.sort(function(a, b) {
            return (a.competition_player.playingPosition < b.competition_player.playingPosition) ? -1 : (a.competition_player.playingPosition > b.competition_player.playingPosition) ? 1 : 0;
        });

        this.sortNames = pointsL;
        this.sortPositions = positionL;
        this.loading.dismiss();
        this.sortNumbers = pointsN;
        console.log(this.sortPositions, 'test position sort');
        console.log(this.sortNumbers, 'hear');
      }, err => {
        console.log(err); 
        this.presentConfirm();
      });
  })
  .catch( error => {
  console.log('error');
  this.presentConfirm();  
  })
    }

  playerHeight(sortNumber) {
    let realFeet = sortNumber.height / 30.48; 
    let feet = Math.floor(realFeet);
    let inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + ' ' + inches + '"';
  }

  sortNameHeight(sortName) {
    let realFeet = sortName.height / 30.48; 
    let feet = Math.floor(realFeet);
    let inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + ' ' + inches + '"';
  }

  sortPositionHeight(sortPosition) {
    let realFeet = sortPosition.height / 30.48; 
    let feet = Math.floor(realFeet);
    let inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + ' ' + inches + '"';
  }

  //palyer cut firstname 
  playerName(sortNumber) {
    let c = sortNumber.firstName;
    let get = sortNumber.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    let d = c.split(" ");
    let e = d[0];
    return c + ' ' + upper;
  }

  //go to player details page
  playerDetailsPoints(sortNumber) {
    this.pass = sortNumber.personId;
    this.navCtrl.push(PlayerDetailPage, { post: this.pass });
  }

  //go to player details page pass by sort name
  playerDetailsPointsSortNames(sortName) {
    this.pass = sortName.personId;
    this.navCtrl.push(PlayerDetailPage, { post: this.pass });
  }

   // load animation gif
   presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.getPlayers();
  }

   //alert if no internet
   presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  //palyer cut firstname sort name
  playerNameSortName(sortName) {
    let c = sortName.firstName;

    let get = sortName.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    let d = c.split(" ");
    let e = d[0];
    return c + ' ' + upper;
  }

  //palyer cut firstname sort position
  playerNameSortPosition(sortPosition) {
    let c = sortPosition.firstName;
    let get = sortPosition.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    let d = c.split(" ");
    let e = d[0];
    return c + ' ' + upper;
  }


}
