import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HomePage } from '../home/home';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-detail-news',
  templateUrl: 'detail-news.html',
})
export class DetailNewsPage {

  public post: any = [];

  datas : any;
  
  array : any;

  ey : number;

  juan : any;

  gets : any;

  constructor(private socialSharing: SocialSharing, public api: ApiProvider, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.getPosts();
    this.post = navParams.get('post');
    
    this.juan =  navParams.get('test');
    console.log(this.juan);
    console.log('dimapalagay');
    console.log(this.post.link);
  }

  share() {
    this.socialSharing.share(this.post.link)
    .then(()=> {

    }).catch(()=> {

    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailNewsPage');
   // this.viewCtrl.setBackButtonText('');
  }

  

  getPosts() {
    this.api.load()
    .then(data => {
      this.datas = data;
      console.log(this.datas);
    });
  }

  doYourStuff() {
    this.navCtrl.popToRoot();
   
  }

  pota(datas) {
    this.ey = this.juan;
    this.ey = this.ey + 1;
    let gets = datas[this.ey];
    if(gets == null) {
      return datas[0].date;
    } else {
      return datas[this.ey].date;
    }
  }

  nextTitle(datas) {
    this.ey = this.juan;
    this.ey = this.ey + 1;
    let gets = datas[this.ey];
    if(gets == null) {
      return datas[0].title.rendered;
    } else {
      return datas[this.ey].title.rendered;
     
    }
  }

  nextImage(datas) {
    this.ey = this.juan;
    this.ey = this.ey + 1;
    let gets = datas[this.ey];
    if(gets == null) {
      return datas[0]._embedded['wp:featuredmedia'][0].media_details.sizes.full.source_url;
    } else {
      return datas[this.ey]._embedded['wp:featuredmedia'][0].media_details.sizes.full.source_url;
     
    }
    
  }


  openNext(datas) {
    this.ey = this.juan;
    this.ey = this.ey + 1;
    this.gets = datas[this.ey];

    if(this.gets == null) {
      this.juan = 0;
      this.gets = datas[0];
    } else {
      this.juan = this.juan + 1;
      this.gets = datas[this.ey];
    }
  
    this.navCtrl.push(DetailNewsPage, { post:this.gets, test:this.juan});
    console.log(this.gets);
  }

}
