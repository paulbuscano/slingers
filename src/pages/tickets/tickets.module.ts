import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicketsPage } from './tickets';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(TicketsPage),
  ],
})
export class TicketsPageModule {}
