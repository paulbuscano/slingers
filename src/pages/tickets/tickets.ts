import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading, LoadingController } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';


@IonicPage()
@Component({
  selector: 'page-tickets',
  templateUrl: 'tickets.html',
})
export class TicketsPage {

  loading: any;

  constructor(private LoadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,private iab: InAppBrowser) {
   this.presentLoader();
  }

  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();

    setTimeout(() => {
      this.loading.dismiss();
    }, 5000);
  }

}
