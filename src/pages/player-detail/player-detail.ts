import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';
import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
import { ModalSocialPage } from '../modal-social/modal-social';

@IonicPage()
@Component({
  selector: 'page-player-detail',
  templateUrl: 'player-detail.html',
})
export class PlayerDetailPage {

  fuck: any;
  lets: any;
  datas: any = [];
  inches: any;
  delta: any;
  feet: any;
  teamID = '79';
  playerStats: any;
  playerLogs: any;
  playerHistories: any;
  teams: any;
  b: any;
  mostMin: any;
  mostPoints: any;
  mostAssists: any;
  mostRebounds: any;
  mostBlocks: any;
  mostSteals: any;
  mostTurnovers: any;
  mostPersonalfouls: any;
  ikaw: any;
  mostFg: any;
  ikawThreept: any;
  mostThreept: any;
  ikawFt: any;
  mostFt: any;
  basag: any;
  loading: any;
  datasCompe: any;
  dataTest: any;
  dataFinal: any;
  compeId: any;
  app: any;
  hasTwitter: any;
  hasFacebook : any;
  playerStatsLength : any;
  playerLogsLength : any;
  facebookshit: any;
  hasInstagram : any;


  constructor(private modal: ModalController, public themeableBrowser: ThemeableBrowser, private appAvailability: AppAvailability, public api: ApiProvider, private alertCtrl: AlertController, private LoadingCtrl: LoadingController, private platform: Platform, public http: Http, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.fuck = navParams.get('post');
    this.presentLoader();
   
  }

  //pass data to modal page 
  openModal(data) {
    //this.navCtrl.push(ModalPage, { post:data });
    const myModal = this.modal.create(ModalSocialPage, { post: data });

    myModal.present();
  }
 


  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayerDetailPage');
    this.viewCtrl.setBackButtonText('');
  }

  twitter(data){
    let app;
    let twitterIdtest = data.nickName;
    let res = twitterIdtest.split("-");
    let twitterId = res[1];
    if (this.platform.is('ios')) {
      app = 'twitter://';
    } else if (this.platform.is('android')) {
      app = 'com.twitter.android';
    }

    this.appAvailability.check(app)
      .then(
      (yes: boolean) => this.hasTwitter ='twitter:///user?screen_name='+ twitterId +'',
     
      (no: boolean) => this.hasTwitter = 'https://twitter.com/'+ twitterId +''
      );

      window.open(this.hasTwitter, '_system', 'location=yes'); return false; 
  }

  facebook(data){
    let app;
    let facebookIdtest = data.nickName;
    let res = facebookIdtest.split("-");
    let facebookId = res[0];
    if (this.platform.is('ios')) {
      app = 'fb://';
    } else if (this.platform.is('android')) {
      app = 'com.facebook.android';
    }

    // this.appAvailability.check(app)
    //   .then(
    //   (yes: boolean) => this.hasFacebook ='fb://profile/'+ facebookId,
     
    //   (no: boolean) => this.hasFacebook = 'https://www.facebook.com/'+ facebookId);

       window.open('fb://profile/'+ facebookId, '_system', 'location=yes'); return false;

  }

  instagram(data){
    let app;
    let instagramTest = data.nickName;
    let res = instagramTest.split("-");
    let instagramId = res[2];
    if (this.platform.is('ios')) {
      app = 'instagram://';
    } else if (this.platform.is('android')) {
      app = 'com.instagram.android';
    }

    this.appAvailability.check(app)
      .then(
      (yes: boolean) => this.hasInstagram ='instagram://user?username='+ instagramId +'',
     
      (no: boolean) => this.hasInstagram = 'http://instagram.com/'+ instagramId +''
      );

      window.open(this.hasInstagram, '_system', 'location=yes'); return false;
  }

  //get player match log
  getTeams() {

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/leagues/6/teams?ak=eebd8ae256142ac3fd24bd2003d28782&limit=100')
      .map(res => res.json())
      .subscribe(data => {
        this.teams = data.response.data;

        console.log('test teams');
        console.log(this.teams);
      }, err => {
        console.log(err);
      });
  }

  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
        <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
      </div>`
    });
    this.loading.present();
    this.getTeams();
    this.getPlayerDetails();
    this.getPlayerStats();
    this.getPlayerMatchLog();
    this.getPlayerHistory();
  }

  //alert if no internet
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  //get player match log
  getPlayerHistory() {

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/statistics/personcompetition/persons/' + this.fuck + '/linkages?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0&limit=20')
      .map(res => res.json())
      .subscribe(data => {
        this.playerHistories = data.response.data;

        let sum = 0;
        let sumPoints = 0;
        let sumAssists = 0;
        let sumRebounds = 0;
        let sumfg = 0;
        let sumBlocks = 0;
        let sumSteals = 0;
        let sumThreepts = 0;
        let sumFt = 0;
        let sumTurnovers = 0;
        let sumPersonalfouls = 0;

        for (var i = 0; i < this.playerHistories.length; i++) {
          sum += this.playerHistories[i].sMinutesAverage;
          sumPoints += this.playerHistories[i].sPointsAverage;
          sumAssists += this.playerHistories[i].sAssistsAverage;
          sumRebounds += this.playerHistories[i].sReboundsTotalAverage;

          let fg = this.playerHistories[i].sFieldGoalsPercentage;
          let n = fg.toString();
          if (n.length < 2) {
            if (n == 1) {
              this.ikaw = n + '00';
            } else {
              this.ikaw = n;
            }
          } else {
            let result = n.slice(2, 4);
            if (result.length < 2) {
              this.ikaw = result + '0';
            } else {
              this.ikaw = result;
            }
          }

          let fgThreept = this.playerHistories[i].sThreePointersPercentage;
          let nThreept = fgThreept.toString();
          if (nThreept.length < 2) {
            if (nThreept == 1) {
              this.ikawThreept = nThreept + '00';
            } else {
              this.ikawThreept = nThreept;
            }
          } else {
            let resultThreept = nThreept.slice(2, 4);
            if (resultThreept.length < 2) {
              this.ikawThreept = resultThreept + '0';
            } else {
              this.ikawThreept = resultThreept;
            }
          }

          let fgFt = this.playerHistories[i].sFreeThrowsPercentage;
          let nFt = fgFt.toString();
          if (nFt.length < 2) {
            if (nFt == 1) {
              this.ikawFt = nFt + '00';
            } else {
              this.ikawFt = nFt;
            }
          } else {
            let resultFt = nFt.slice(2, 4);
            if (resultFt.length < 2) {
              this.ikawFt = resultFt + '0';
            } else {
              this.ikawFt = resultFt;
            }
          }

          sumfg += parseInt(this.ikaw);
          sumBlocks += this.playerHistories[i].sBlocksAverage;
          sumSteals += this.playerHistories[i].sStealsAverage;
          sumThreepts += parseInt(this.ikawThreept);
          sumFt += parseInt(this.ikawFt);
          sumTurnovers += this.playerHistories[i].sTurnoversAverage;
          sumPersonalfouls += this.playerHistories[i].sFoulsPersonalAverage;
        }
        let sumtrim: any = sum.toFixed(2).slice(0, -1);
        let sumPointstrim: any = sumPoints.toFixed(2).slice(0, -1);
        let sumAssiststrim: any = sumAssists.toFixed(2).slice(0, -1);
        let sumReboundstrim: any = sumRebounds.toFixed(2).slice(0, -1);
        let sumFgtrim: any = sumfg;
        let sumBlockstrim: any = sumBlocks.toFixed(2).slice(0, -1);
        let sumStealstrim: any = sumSteals.toFixed(2).slice(0, -1);
        let sumThreeptstrim: any = sumThreepts;
        let sumFttrim: any = sumFt;
        let sumTurnoverstrim: any = sumTurnovers.toFixed(2).slice(0, -1);
        let sumPersonalfoulstrim: any = sumPersonalfouls.toFixed(2).slice(0, -1);


        let avg = sumtrim / this.playerHistories.length;
        let avgPoints = sumPointstrim / this.playerHistories.length;
        let avgAssists = sumAssiststrim / this.playerHistories.length;
        let avgRebounds = sumReboundstrim / this.playerHistories.length;
        let avgFg = sumFgtrim / this.playerHistories.length;
        let avgBlocks = sumBlockstrim / this.playerHistories.length;
        let avgSteals = sumStealstrim / this.playerHistories.length;
        let avgThreepts = sumThreeptstrim / this.playerHistories.length;
        let avgFt = sumFttrim / this.playerHistories.length;
        let avgTurnovers = sumTurnoverstrim / this.playerHistories.length;
        let avgPersonalfouls = sumPersonalfoulstrim / this.playerHistories.length;

        this.mostMin = avg.toFixed(2).slice(0, -1);
        this.mostPoints = avgPoints.toFixed(2).slice(0, -1);
        this.mostAssists = avgAssists.toFixed(2).slice(0, -1);
        this.mostRebounds = avgRebounds.toFixed(2).slice(0, -1);
        this.mostFg = Math.round(avgFg);
        this.mostBlocks = avgBlocks.toFixed(2).slice(0, -1);
        this.mostSteals = avgSteals.toFixed(2).slice(0, -1);
        this.mostThreept = Math.round(avgThreepts);
        this.mostFt = Math.round(avgFt);
        this.mostTurnovers = avgTurnovers.toFixed(2).slice(0, -1);
        this.mostPersonalfouls = avgPersonalfouls.toFixed(2).slice(0, -1);

        console.log('test player history');
        console.log(this.playerHistories);
      }, err => {
        console.log(err);
      });
  }

  //get player match log
  getPlayerMatchLog() {
    this.api.loadLatestCompetitionId()
      .then(data => {
        this.datasCompe = data.response.data;

        //sort api to firstname
        this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
        this.dataFinal = this.dataTest.sort(function (a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
        });
        this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
       // console.log(this.compeId, 'test wew');

        if (this.platform.is('mobile')) {
          this.lets = "https://api.wh.geniussports.com/v1";
        } else {
          this.lets = "/v1";
        }

        this.http.get(this.lets + '/basketball/matchlog/' + this.fuck + '?ak=eebd8ae256142ac3fd24bd2003d28782&competitionId=' + this.compeId + '&limit=50')
          .map(res => res.json())
          .subscribe(data => {
            this.playerLogs = data.response.data;
            this.playerLogsLength = this.playerLogs.length;
            console.log('test player logs');
            console.log(this.playerLogsLength);
          }, err => {
            console.log(err);
          });
      })
      .catch(error => {
        console.log('error');
        this.presentConfirm();
      })
  }

  //get genius by click id
  getPlayerDetails() {

    if (this.platform.is('mobile')) {
      this.lets = "https://api.wh.geniussports.com/v1";
    } else {
      this.lets = "/v1";
    }

    this.http.get(this.lets + '/basketball/persons/' + this.fuck + '/linkages?ak=eebd8ae256142ac3fd24bd2003d28782')
      .map(res => res.json())
      .subscribe(data => {
        this.datas = data.response.data;

        console.log('test player details');
        console.log(this.datas);
      }, err => {
        console.log(err);
      });
  }

  //get genius player stats
  getPlayerStats() {
    this.api.loadLatestCompetitionId()
      .then(data => {
        this.datasCompe = data.response.data;

        //sort api to firstname
        this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
        this.dataFinal = this.dataTest.sort(function (a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
        });
        this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;
        //console.log(this.compeId);

        if (this.platform.is('mobile')) {
          this.lets = "https://api.wh.geniussports.com/v1";
        } else {
          this.lets = "/v1";
        }
//'/basketball/statistics/personround/competitions/' + this.compeId + '/teams/' + this.teamID + '/persons/' + this.fuck + '?ak=eebd8ae256142ac3fd24bd2003d28782&limit=1'
        this.http.get(this.lets + '/basketball/statistics/personcompetition/competitions/' + this.compeId + '/teams/' + this.teamID + '/persons/' + this.fuck + '?ak=eebd8ae256142ac3fd24bd2003d28782&periodNumber=0')
          .map(res => res.json())
          .subscribe(data => {
            this.playerStats = data.response.data;
            this.playerStatsLength =  this.playerStats.length;
            this.loading.dismiss();
            console.log('test player stats');
            console.log(this.playerStatsLength);
          }, err => {
            this.presentConfirm();
            console.log(err);
          });
      })
      .catch(error => {
        console.log('error');
        this.presentConfirm();
      })
  }

  //palyer cut firstname 
  personFirstname(data) {
    let c = data.firstName;
    let d = c.split(" ");
    let e = d[0];
    return c;
  }

  //player last name convert 
  playerLastname(data) {
    let get = data.familyName;
    let g = get.toString();
    let pota = get.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    return upper;
  }

  //convert height 

  playerHeight(data) {
    let realFeet = data.height / 30.48; 
    let feet = Math.floor(realFeet);
    let inches = Math.round((realFeet - feet) * 12);
    return feet + "'" + ' ' + inches + '"';
  }

  //convert kg to lbs 
  playerWeight(data) {
    let a = data.weight;
    if (a == 0) {
      return a;
    } else {
      let b = a * 2.2046;
      let converweight = a.toString();
      let lbs = converweight.split(".")[0];
      return lbs + ' lbs.';
    }
  }


  //convert nationaly to capitalize 
  playerNationality(data) {
    let a = data.nationality;
    let pota = a.toLowerCase();
    let upper = pota.charAt(0).toUpperCase() + pota.substr(1);

    return upper;
  }

  //player bday 
  playerBirthday(data) {
    let todayTime = new Date(data.dob);
    let bday = todayTime.toDateString();
    var newStr = bday.split(' ').slice(1).join(' ');
    return newStr;
  }

  // one decimal places pts
  decimalPpg(playerStat) {
    let a = playerStat.sPointsAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal places pts
  decimalApg(playerStat) {
    let a = playerStat.sAssistsAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal places pts
  decimalRpg(playerStat) {
    let a = playerStat.sReboundsTotalAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  //player fg convert
  playerFgpercentage(playerStat) {
    let a = playerStat.sFieldGoalsPercentage;
    let shit = a.toString();
    let remove = shit.substring(2);
    if (remove.length < 2) {
      return remove + 0 + '%';
    } else {
      return remove + '%';
    }
  }

  //player ft convert
  playerFtpercentage(playerStat) {
    let a = playerStat.sFreeThrowsPercentage;
    let shit = a.toString();
    let remove = shit.substring(2);
    if (remove.length < 2) {
      return remove + 0 + '%';
    } else {
      return remove + '%';
    }

  }

  //player minutes convert
  minutes(playerLog) {
    let getmin = playerLog.sMinutes;
    let n = getmin.toString();
    if (n.length > 3) {
      if (n.length == 4) {
        let bala = n.slice(0, 1);
        return bala;
      } else {
        let result = n.slice(0, 2);
        return result;
      }
    } else {
      return n;
    }
  }

  //player fg convert
  fieldgoal(playerLog) {
    let fg = playerLog.sFieldGoalsPercentage;
    let n = fg.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  threepoints(playerLog) {
    let tp = playerLog.sThreePointersPercentage;
    let n = tp.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  freethrows(playerLog) {
    let tp = playerLog.sFreeThrowsPercentage;
    let n = tp.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player career year 
  careerYear(playerHistorie) {
    let a = playerHistorie.year;
    let b = a.toString();
    let res = b.substr(2, 3);
    let d = parseInt(res);
    let e = d + 1;
    return res + '-' + e;
  }

  //player career team 
  careerTeam(playerHistorie) {
    this.basag = this.teams.length;
    let a = playerHistorie.teamId;
    for (let i = 0; i < this.basag; i++) {
      if (this.teams[i].teamId === a) {
        this.b = this.teams[i].teamCode;
      }
    }
    return this.b;

  }

  //player minutes convert
  minutesCareer(playerHistorie) {
    let getmin = playerHistorie.sMinutesAverage;
    let n = getmin.toString();
    if (n.length > 3) {
      if (n.length == 4) {
        let bala = n.slice(0, 1);
        return bala;
      } else {
        let result = n.slice(0, 2);
        return result;
      }
    } else {
      return n;
    }
  }

  //player fg convert
  fieldgoalCareer(playerHistorie) {
    let fg = playerHistorie.sFieldGoalsPercentage;
    let n = fg.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  threepointsCareer(playerHistorie) {
    let tp = playerHistorie.sThreePointersPercentage;
    let n = tp.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  //player three points convert
  freethrowsCareer(playerHistorie) {
    let tp = playerHistorie.sFreeThrowsPercentage;
    let n = tp.toString();
    if (n.length < 2) {
      if (n == 1) {
        return n + '00';
      } else {
        return n;
      }
    } else {
      let result = n.slice(2, 4);
      if (result.length < 2) {
        return result + '0';
      } else {
        return result;
      }
    }
  }

  // one decimal place min
  decimalMpg(playerHistorie) {
    let a = playerHistorie.sMinutesAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place ppg
  decimalCareerPpg(playerHistorie) {
    let a = playerHistorie.sPointsAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place apg
  decimalCareerApg(playerHistorie) {
    let a = playerHistorie.sAssistsAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place rpg
  decimalCareerRpg(playerHistorie) {
    let a = playerHistorie.sReboundsTotalAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place bpg
  decimalCareerBpg(playerHistorie) {
    let a = playerHistorie.sBlocksAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place spg
  decimalCareerSpg(playerHistorie) {
    let a = playerHistorie.sStealsAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place tpg
  decimalCareerTpg(playerHistorie) {
    let a = playerHistorie.sTurnoversAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }

  // one decimal place fppg
  decimalCareerFppg(playerHistorie) {
    let a = playerHistorie.sFoulsPersonalAverage;
    let b = a.toFixed(2).slice(0, -1);
    return b;
  }



}
