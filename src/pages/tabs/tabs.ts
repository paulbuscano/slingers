import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { TeamPage } from '../team/team';
import { StatisticsPage } from '../statistics/statistics';
import { RosterPage } from '../roster/roster';
import { TrackerPage } from '../tracker/tracker';
import { StandingsPage } from '../standings/standings';
import { GamesPage } from '../games/games';
import { SamplePage } from '../sample/sample';
import { VideosPage } from '../videos/videos';
import { PlayerDetailPage } from '../player-detail/player-detail';
import { GameDetailPage } from '../game-detail/game-detail';
import { PlaybyplayPage } from '../playbyplay/playbyplay';
import { TicketsPage } from '../tickets/tickets';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = TrackerPage;
  tab3Root = VideosPage;
  tab4Root = TeamPage;
  tab5Root = TicketsPage;

  constructor() {

  }
}
