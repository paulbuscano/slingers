import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Loading, LoadingController, AlertController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { GameDetailPage } from '../game-detail/game-detail';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-tracker',
  templateUrl: 'tracker.html',
})
export class TrackerPage {

  lets = "/v1";
  dataMatches : any;
  loading : any;
  dataTest : any;
  dataFinal : any;
  compeId : any;
  datasCompe : any;
  teamId = '79';
  month : any;
  
  contacts : any;;
  groupedContacts : any=[];
  ahh : any=[];
  dataMatchesTest : any;
  dateOnly : any=[];
  hey : any=[];
  dateTitle : any=[];
  newGroup : any;
  dataTestPota : any;
  dataFinalPota : any;
  quarter : any;
  one : any;
  two : any;

  constructor(public api: ApiProvider, private alertCtrl: AlertController, private LoadingCtrl: LoadingController, public http: Http, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,private platform: Platform) {
    this.presentLoader();
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrackerPage');
    this.viewCtrl.setBackButtonText('');
  }

   //pass data to game details page
  gameDetails(tae) {
    this.navCtrl.push(GameDetailPage, { post:tae, teamOne:tae.competitors[0].competitorId, teamTwo:tae.competitors[1].competitorId, match:tae.matchId });
  }

  //get matches
  groupContacts(){

    this.api.loadLatestCompetitionId()
    .then( data => {
      this.datasCompe = data.response.data;
    
      //sort api to firstname
      this.dataTest = JSON.parse(JSON.stringify(this.datasCompe));
      this.dataFinal =  this.dataTest.sort(function(a, b) {
          return (a.year < b.year) ? -1 : (a.year > b.year) ? 1 : 0;
      });
      this.compeId = this.dataFinal[this.dataFinal.length - 1].competitionId;

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }

      this.http.get(this.lets + '/basketball/competitions/'+ this.compeId +'/matcheslive?ak=eebd8ae256142ac3fd24bd2003d28782&teamId='+ this.teamId +'&limit=40')
      .map(res => res.json())
      .subscribe(data => {
        this.dataMatchesTest = data.response.data;
        
        //sort matches to date descending
        this.dataTestPota = JSON.parse(JSON.stringify(this.dataMatchesTest));
        this.dataFinalPota =  this.dataTestPota.sort(function(a, b) {
            return (a.matchTime < b.matchTime) ? -1 : (a.matchTime > b.matchTime) ? 1 : 0;
        });
       
        let currentLetter = false;
        let currentContacts = [];
        let currentDatas = [];

    for(let i = 0; i < this.dataFinalPota.length; i++) {
        this.ahh = this.dataFinalPota[i].matchTime.slice(0,7);
        if(this.ahh != currentLetter) {
          currentLetter = this.ahh;
          let newGroup = {
                letter: currentLetter,
                contacts: [],
                taes: []
            };
           currentContacts = newGroup.contacts;
           currentDatas = newGroup.taes;
            this.groupedContacts.push(newGroup);
          }
        currentContacts.push(this.ahh);
        currentDatas.push(this.dataFinalPota[i]);
    }
      console.log(this.groupedContacts, 'asdf');
      
      this.loading.dismiss();
      }, err => {
        console.log(err);
        this.loading.dismiss();
        this.presentConfirm();
      });
    })
    .catch( error => {
      console.log('error');
      this.presentConfirm();
    })



}


  // load animation gif
  presentLoader() {
    this.loading = this.LoadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
      <img class="loading" width="120px" height="120px" src="assets/imgs/sample/animate-slingers.gif" />
    </div>`
    });
    this.loading.present();
    this.groupContacts();
  }

   //alert if no internet
   presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'No Internet Connection',
      message: 'Please check your internet connection and try again',
      buttons: [
        {
          text: 'Try Again',
          handler: () => {
            console.log('try again clicked');
            this.loading.dismiss();
            this.presentLoader();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('cancel clicked');
            this.loading.dismiss();
          }
        }
      ]
    });
    alert.present();
  }

  liveClock(tae) {
    let periodType = tae.live.periodType;
      if(periodType == 'REGULAR') {
          this.quarter = 'Q' + tae.live.period;
      } else {
          this.quarter = 'OT' + tae.live.period;
      }
    let time = tae.live.clock;
    let trimTime = time.split(':');
    return this.quarter + ' ' + trimTime[0] + ':' + trimTime[1];
  } 
  liveScoreZero(tae) {
    this.one = 1;
    let naOne = this.one.toString();
    let zero = tae.live.scores.naOne;
    return zero;
  }
  liveScoreOne(tae) {
    this.two = 2;
    let naTwo = this.two.toString();
    let zero = tae.live.scores.naTwo;
    return zero;
  }

}
