import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackerPage } from './tracker';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(TrackerPage),
  ],
})
export class TrackerPageModule {}
