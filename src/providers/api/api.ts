import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import {Http , Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiProvider {

  data : any;

  dataYoutube : any;

  dataMatches : any;
  dataCompetition : any;
  lets : any;

  getApiUrl : string = "http://www.singaporeslingers.com/wp-json/wp/v2/posts?_embed&per_page=20";

 // youtubeApi : string = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyBU7865vDB9p9APyxKtJC2bjrpf7XqiVbo&channelId=UCjFguEjaNj6L0xbRqEpFqYg&part=snippet,id&order=date&maxResults=20";

  youtubeApi : string = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=30&playlistId=PLs5fUaaDEGQxoEX1PE_hfa3x-QaqgJNrR&key=AIzaSyBU7865vDB9p9APyxKtJC2bjrpf7XqiVbo";

  getCompetitionUrl : string = "/basketball/leagues/6/competitions?ak=eebd8ae256142ac3fd24bd2003d28782&limit=20";

  constructor(public http: Http,private platform: Platform) {
    console.log('Hello ApiProvider Provider');
  }

  load() {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
  
    // don't have the data yet
    return new Promise((resolve, reject) => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      this.http.get(this.getApiUrl)
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.data = data;
        
          resolve(this.data);
        }, err => {
          reject(err);
        });
    });
  }

  

  loadYoutube() {
    if (this.dataYoutube) {
      // already loaded data
      return Promise.resolve(this.dataYoutube);
    }
  
    // don't have the data yet
    return new Promise((resolve, reject) => {
      // We're using Angular HTTP provider to request the data,
      // then on the response, it'll map the JSON data to a parsed JS object.
      // Next, we process the data and resolve the promise with the new data.
      this.http.get(this.youtubeApi)
        .map(res => res.json())
        .subscribe(data => {
          // we've got back the raw data, now generate the core schedule data
          // and save the data for later reference
          this.dataYoutube = data;
        
          resolve(this.dataYoutube);
        }, err => {
          reject(err);
        });
    });
  }

  loadLatestCompetitionId() {
    if (this.dataCompetition) {
      // already loaded data
      return Promise.resolve(this.dataCompetition);
    }
  
    // don't have the data yet
    return new Promise((resolve, reject) => {

      if (this.platform.is('mobile')) {
        this.lets = "https://api.wh.geniussports.com/v1";
      } else {
        this.lets = "/v1";
      }
      this.http.get(this.lets + this.getCompetitionUrl)
        .map(res => res.json())
        .subscribe(data => {
          this.dataCompetition = data;    
          resolve(this.dataCompetition);
        }, err => {
          reject(err);
        });
    });
  }

 

}
