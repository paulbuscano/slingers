import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { OneSignal } from '@ionic-native/onesignal';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = TabsPage;
  singal_app_id:string = '7f787c1a-09c9-49ef-a56d-f415c7b3d019';
  // singal_app_id:string = '3a22e844-a73d-4eaa-92af-69ff905e336c';
  
  firebase_id:string = '448094113074';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleLightContent();
      // let status bar overlay webview
      //statusBar.overlaysWebView(true);
      // set status bar to white
      //statusBar.backgroundColorByHexString('#ffffff');
     
      splashScreen.hide();

      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };


      window["plugins"].OneSignal
        .startInit(this.singal_app_id, this.firebase_id)
        .handleNotificationOpened(notificationOpenedCallback)
        // .sendTags({
        //   latitude: 14.5571963,
        //   long: 121.00696984
        // })
        .endInit();
      window["plugins"].OneSignal.setSubscription(true);
      window["plugins"].OneSignal.promptLocation();
    });
  }
}
