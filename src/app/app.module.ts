import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
//add by me 
import { TeamPage } from '../pages/team/team';
import { StatisticsPage } from '../pages/statistics/statistics';
import { RosterPage } from '../pages/roster/roster';
import { TrackerPage } from '../pages/tracker/tracker';
import { StandingsPage } from '../pages/standings/standings';
import { DetailNewsPage } from '../pages/detail-news/detail-news';
import { GamesPage } from '../pages/games/games';
import { SamplePage } from '../pages/sample/sample';
import { VideosPage } from '../pages/videos/videos';
import { PlayerDetailPage } from '../pages/player-detail/player-detail';
import { GameDetailPage } from '../pages/game-detail/game-detail';
import { PlaybyplayPage } from '../pages/playbyplay/playbyplay';
import { ModalPage } from '../pages/modal/modal';
import { TicketsPage } from '../pages/tickets/tickets';
import { ModalSocialPage } from '../pages/modal-social/modal-social';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';

import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
// import { LazyLoadImageModule } from 'ng2-lazyload-image';
import { MomentPipe } from '../pipes/moment/moment';
import { YoutubePipe } from '../pipes/youtube/youtube';
import { DateforlogPipe } from '../pipes/dateforlog/dateforlog';
import { DatefortrackerPipe } from '../pipes/datefortracker/datefortracker';
import { DatefortackerlabelPipe } from '../pipes/datefortackerlabel/datefortackerlabel';
import { GroupByPipe } from '../pipes/group-by/group-by';
import { DateforteamPipe } from '../pipes/dateforteam/dateforteam';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';
import { ThemeableBrowser } from '@ionic-native/themeable-browser';
import { SocialSharing } from '@ionic-native/social-sharing';

import { LazyLoadImageModule } from 'ng-lazyload-image';
import { OneSignal } from '@ionic-native/onesignal';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    TeamPage,
    StatisticsPage,
    RosterPage,
    TrackerPage,
    StandingsPage,
    DetailNewsPage,
    GamesPage,
    SamplePage,
    VideosPage,
    PlayerDetailPage,
    GameDetailPage,
    PlaybyplayPage,
    MomentPipe,
    YoutubePipe,
    ModalPage,
    DateforlogPipe,
    DatefortrackerPipe,
    DatefortackerlabelPipe,
    TicketsPage,
    ModalSocialPage,
    GroupByPipe,
    DateforteamPipe,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    // LazyLoadImageModule,
    LazyLoadImageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    TeamPage,
    StatisticsPage,
    RosterPage,
    TrackerPage,
    StandingsPage,
    DetailNewsPage,
    GamesPage,
    SamplePage,
    VideosPage,
    PlayerDetailPage,
    GameDetailPage,
    PlaybyplayPage,
    ModalPage,
    TicketsPage,
    ModalSocialPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    YoutubeVideoPlayer,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiProvider,
    Network,
    InAppBrowser,
    AppAvailability,
    ThemeableBrowser,
    SocialSharing,
    ScreenOrientation,
  ]
})
export class AppModule { }
